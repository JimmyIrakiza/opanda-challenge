const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");
const db = require("./config/database");

//Testing Connection
db.authenticate()
  .then(() => console.log("Conected to DB"))
  .catch((err) => console.log("failed to connect " + err));

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/", require("./routes/quizes"));

app.listen(3001, () => {});
