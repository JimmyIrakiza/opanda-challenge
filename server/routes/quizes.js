const express = require("express");
const router = express.Router();
const db = require("../config/database");
const Quiz = require("../models/Quiz");

//list of quizes
router.get("/quizes", (req, res) =>
  Quiz.findAll()
    .then((quizes) => {
      res.send(quizes);
    })
    .catch((err) => console.log(err))
);

//Get One Quiz
router.get("/quizes/:id", (req, res) => {
  const q = req.params.id;
  //const v = q.substr(1);
  Quiz.findByPk(parseInt(q))
    .then((quiz) => {
      res.send(quiz);
    })
    .catch((err) => console.log(err));
});

//add a quiz
router.post("/add-quiz", (req, res) => {
  const data = {
    quizName: req.body.quizName,
    quizCode: req.body.quizCode,
    questionOne: req.body.questionOne,
    questionTwo: req.body.questionTwo,
    questionThree: req.body.questionThree,
    questionFour: req.body.questionFour,
    questionFive: req.body.questionFive,
    answerOne: req.body.answerOne,
    answerTwo: req.body.answerTwo,
    answerThree: req.body.answerThree,
    answerFour: req.body.answerFour,
    answerFive: req.body.answerFive,
  };
  let {
    quizName,
    quizCode,
    questionOne,
    questionTwo,
    questionThree,
    questionFour,
    questionFive,
    answerOne,
    answerTwo,
    answerThree,
    answerFour,
    answerFive,
  } = data;
  Quiz.create({
    quizName,
    quizCode,
    questionOne,
    questionTwo,
    questionThree,
    questionFour,
    questionFive,
    answerOne,
    answerTwo,
    answerThree,
    answerFour,
    answerFive,
  })
    .then((quiz) => res.redirect("/"))
    .catch((err) => console.log(err));
});

module.exports = router;
