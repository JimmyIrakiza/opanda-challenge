const Sequelize = require("sequelize");
const db = require("../config/database");

const Quiz = db.define("quiz", {
  quizName: {
    type: Sequelize.STRING,
    allowNull: false,
  },

  quizCode: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  questionOne: {
    type: Sequelize.STRING,
  },
  questionTwo: {
    type: Sequelize.STRING,
  },
  questionThree: {
    type: Sequelize.STRING,
  },
  questionFour: {
    type: Sequelize.STRING,
  },
  questionFive: {
    type: Sequelize.STRING,
  },
  answerOne: {
    type: Sequelize.STRING,
  },
  answerTwo: {
    type: Sequelize.STRING,
  },
  answerThree: {
    type: Sequelize.STRING,
  },
  answerFour: {
    type: Sequelize.STRING,
  },
  answerFive: {
    type: Sequelize.STRING,
  },
});

module.exports = Quiz;
